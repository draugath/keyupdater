declare("KeyUpdater", {update_key_info = {}})
local tc, ti, tr = table.concat, table.insert, table.remove

-- Hook the KeyInfoDialog.show_cb function to allow it to be opened and closed without interrupting the game
local old_show_cb = KeyInfoDialog.show_cb
function KeyInfoDialog:show_cb(...)
	if (type(old_show_cb) == 'function') then old_show_cb(...) end
	if (#KeyUpdater.update_key_info > 0) then
		tr(KeyUpdater.update_key_info, 1)
		HideDialog(self)
	end
end

function KeyUpdater:refresh_key(keyid)
	ti(self.update_key_info, keyid)
	Keychain.list(keyid) -- open the KeyInfoDialog to refresh the key data
end

function KeyUpdater.key_refreshed(_, data)
	if (data.msg == 'Key info received.' or data.msg == 'Error: You do not have that Key.') then
		tr(KeyUpdater.update_key_info, 1)
	end
end	

RegisterEvent(KeyUpdater.key_refreshed, "CHAT_MSG_SERVER")
-- --------------------------------------------------------

function KeyUpdater:get_active_keys_info(owner_only, get_inactive)
	owner_only = owner_only or false

	-- Get list of active keys on the keychain
	-- If there is both a user and owner key, only get the owner info
	-- if owner_only == true, only return owner key info
	
	-- linked stations/capships and user lists are only updated when the key is refreshed, nil until them
	
	local keychain = {}
	for index = 1, GetNumKeysInKeychain() do
		local keyid, name, ownerid, _, _, _, isactive = GetKeyInfo(index)
		-- get the list of active keys
		if (isactive or get_inactive) then
			keychain[tonumber(keyid)] = {index = index, owner = ownerid} -- the keyid field is returned as a string.  force it to a number
		end
	end
	-- remove user-keys that have a corresponding owner-key
	local user_list = {}
	for k, v in pairs(keychain) do
		if ( v.owner and (keychain[v.owner] or owner_only) ) then user_list[v.owner] = {uid = k, index = v.index} end
	end
	-- Cannot trim the key list until after the user keys have been refreshed
	-- ***  This change was made because I needed to have the userkeys updated so I could properly check and refresh permissions.
	-- ***  I just didn't make it properly mesh with the original paradigm of customizable returns
	-- ***  If you don't need to check any values on user keys, then you can swap the trim location, or adjust it as necessary
	--for _, v in pairs(user_list) do keychain[v.uid] = nil end

	-- refesh active key data
	for keyid in pairs(keychain) do self:refresh_key(keyid) end

	-- wait for refresh
	coroutine.yield()

	-- Now trim the key list
	for _, v in pairs(user_list) do keychain[v.uid] = nil end
	
	-- compile list of keys and associated stations
	local activekeys = {}
	local inactivekeys = {}
	for id, key in pairs(keychain) do
		local keyinfo = {GetKeyInfo(key.index)} -- 2 = description, 5 = stations, 7 = isactive
		keyinfo[5] = keyinfo[5] or {}

		if ( next(keyinfo[5]) ) then
			local s_names = {}
			for i, v in ipairs(keyinfo[5]) do 
				s_names[i] = v[1]['name'] 
			end
			
			local str = (not keyinfo[7] and '* ' or '')..keyinfo[2]..' ('..tc(s_names, ', ')..')'

			-- Not sure if I need this anymore.  keeping it for now.
-- 			if (keyinfo[7]) then
 				activekeys[id] = str
-- 			else
-- 				inactivekeys[id] = str
-- 			end
		end
	end

	return activekeys, user_list, inactivekeys
end

function KeyUpdater:get_keys(func, owner_only, get_inactive)
	func = func or printtable
	--func should be a function that receives one argument,
	--a table of the form: {[keyid] = "Key_Name ([Station_Name1[, ...]])"}
	
	owner_only = owner_only or false
	local co_timer = Timer()
	local co = coroutine.create(function(owner_only)
		func(self:get_active_keys_info(owner_only, get_inactive))
	end)

	local _, state = coroutine.resume(co, owner_only)
	
	-- wait for all of the Keychain dialogs to be opened and closed before proceeding
	local function co_loop()
		if (#KeyUpdater.update_key_info > 0) then
			co_timer:SetTimeout(50, co_loop)
		else
			coroutine.resume(co)
		end
	end
	
	co_loop()
end


function KeyUpdater.printkeys(display_func, owner_only, get_inactive)
	-- this is a wrapper function for backwards compatibility until I can reimplement the new function calls in dependant plugins
	owner_only = owner_only or false
	display_func = display_func or console_print
	--display_func should be a function that receives one argument: function(str) generalprint(str) end
	
	local function func(keys)
		local info = {}
		for _, key in pairs(keys) do ti(info, key) end
		table.sort(info)
		display_func("Active Keys: "..table.concat(info, ". "))
	end
	KeyUpdater:get_keys(func, owner_only, get_inactive)
end
